#include <stdio.h>
#include <unistd.h>

//function declaration
void display(int arr[], int size, int j, int jgap );
void shellSort(int arr[], int size);

    int arr[] = { 6, 4, 5, 7, 0, 2, 1, 8,3,9 }; //unsorted elements


    int n = sizeof(arr)/sizeof(arr[0]); //size of the array

int main(void) {


  
  display(arr, n,-1,-1); //output unsorted elements

  shellSort(arr, n); //sort the elements


//    display(arr, n,-1,-1); //display sorted elements


  return 0;
}

void display_r(int arr[], int size, int j, int jgap) {
  int i;
  printf("\r");
  sleep( 1 );
  for(i = 0; i < size; i++) {
    if( j == i || jgap == i ) {
      printf("\033[0;31m"); 
      printf("%d ", arr[i]);
      printf("\033[0m");
    }
    else {
      printf("%d ", arr[i]);
    }
  }
  fflush( stdout );
}

void display(int arr[], int size, int j, int jgap) {
  int i;
  sleep( 1 );
  for(i = 0; i < size; i++) {
    if( j == i || jgap == i ) {
      printf("\033[0;31m"); 
      printf("%d ", arr[i]);
      printf("\033[0m");
    }
    else {
      printf("%d ", arr[i]);
    }
  }
  printf("\n");
  fflush( stdout );
}

void shellSort(int arr[], int size) {
  int i, j, gap, temp, jgap;

  //we start with a bigger gap
  gap = size/2;

  while(gap > 0) {
    //now we will do the insertion sort of the gapped elements
    i = gap;

    while(i < size) {
      temp = arr[i];

      //shifting gap sorted element to correct location
      for(j = i; (j >= gap) && (arr[j - gap] > temp); j -=gap) {
        jgap = j-gap;
        arr[j] = arr[j - gap];
        //display(arr, n, j, jgap);
      }
      arr[j] = temp;
      display(arr, n, i, jgap);

      //increase i
      i++;
    }

    //reduce the gap by half
    gap = gap / 2;
  }
}