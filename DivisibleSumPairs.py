n = 6 
k = 3 
ar = [1, 3, 2, 6, 1, 2]
def divisibleSumPairs(n, k, arr):
    count = 0
    for a in range(0, n):
        for b in range(a+1, n):
            if (ar[a] + ar[b]) % k == 0:
                count += 1
    return(count)

h = divisibleSumPairs(n, k, ar)
print(h)
